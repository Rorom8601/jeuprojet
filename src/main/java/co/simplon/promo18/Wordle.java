package co.simplon.promo18;

public class Wordle {
  GameMode gameMode = new GameMode();
  Printer printer = new Printer();
  public String wordToGuess;
  public String wordTried;
  public char[] tabWordTried;
  public int nbTry = 1;
  public int letterGuessed = 0;
  public char[] lettresTentees = new char[5];
  public char[] tabWordToGuess = new char[5];
  public char[] tabWordGuessedSoFar = {'*', '*', '*', '*', '*'};

  public void Start() {
    printer.printStartMessage(gameMode);
    printer.getWordToGuess(gameMode, this);
    printer.printMessageForPlayer();
    while (this.nbTry <= 7) {
      reinitializeWordToFind();
      initializeWordGuessedSoFar();
      printer.printTryNumber(this.nbTry);
      this.tabWordTried = printer.getTabWordTried(this.wordTried);
      checkForLetterInRightPlace();
      checkForLetterInWrongPlace();
      incrementTry();
      printer.printReponse(this.tabWordGuessedSoFar);
      reinitializeLetterGuessed();
      checkVictoryCondition();
      printer.checkDefeatCondition(this);
      printer.startNewLine();
    }
  }

  public void reinitializeLetterGuessed() {
    this.letterGuessed = 0;
  }

  public void initializeWordGuessedSoFar() {

    for (int i = 0; i < this.tabWordGuessedSoFar.length; i++) {
      this.tabWordGuessedSoFar[i] = '*';
    }
  }

  public void reinitializeWordToFind() {
    this.tabWordToGuess = wordToGuess.toCharArray();
  }

  public void incrementTry() {
    this.nbTry++;
  }

  public void checkForLetterInRightPlace() {

    for (int i = 0; i < this.tabWordToGuess.length; i++) {
      for (int j = 0; j < this.tabWordTried.length; j++) {
        if (this.tabWordTried[j] == this.tabWordToGuess[i]) {
          if (i == j) {
            // Mettre la lettre trouvé à la bonne place
            // dans le nouveau mot deviné en majuscule
            this.tabWordGuessedSoFar[i] = Character.toUpperCase(tabWordTried[i]);
            // Remplacer le caractere trouvé par une *
            this.tabWordToGuess[i] = '*';
          }
        }
      }
    }
  }

  public void checkForLetterInWrongPlace() {
    for (int i = 0; i < tabWordToGuess.length; i++) {
      for (int j = 0; j < tabWordTried.length; j++) {
        if (this.tabWordToGuess[i] == this.tabWordTried[j]) {
          // Remplacer le caractere trouvé à une mauvaise place
          // par le caractère en minuscule
          // Ce if gère le cas des doublons
          if (this.tabWordGuessedSoFar[j] == '*') {
            this.tabWordGuessedSoFar[j] = tabWordTried[j];
            // Remplacer le caractere trouvé par une #
            this.tabWordToGuess[i] = '#';
          }
        }
      }

    }
  }

  public void checkVictoryCondition() {
    for (int i = 0; i < this.tabWordGuessedSoFar.length; i++) {
      if (Character.isUpperCase(this.tabWordGuessedSoFar[i])) {
        this.letterGuessed++;
      }
    }
    // Condition de victoire
    if (this.letterGuessed >= 5) {
      System.out.println("\nVous avez gagné, félicitations!!!");
      printer.askToPlayAgain(this);
    }
  }

}
