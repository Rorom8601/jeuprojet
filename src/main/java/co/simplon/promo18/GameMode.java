package co.simplon.promo18;

import java.util.Scanner;

public class GameMode {

 
  public boolean TwoPLayers;
  public Scanner scan;

  public void getScanner() {
    scan = new Scanner(System.in);
  }

  public void selectGameMode(){
    System.out.println("Tapez m si vous voulez faire deviner votre mot à votre adversaire, ");
    System.out.println("tapez s si vous voulez jouer seul et tenter de deviner un mot choisi aléatoirement");
    if (scan == null) {
      getScanner();
    }
    String answer=scan.nextLine();
    while(!(answer.equals("m") || answer.equals("s"))){
      System.out.println("Je n'ai pas compris votre réponse, tapez s pour jouer seul, ou m pour jouer à deux");
      answer=scan.nextLine();

    }
    if(answer.equals("m")){
      this.TwoPLayers=true;
    }
    if(answer.equals("s")){
      this.TwoPLayers=false;
    }
    // if(answer!="m" &&answer!="s")
    // {
    //   System.out.println("Je n'ai pas compris votre réponse, tapez s pour jouer seul, ou m pour jouer à deux");
    //   String answer=scan.nextLine();
    // }
    //Wordle.eraseConsole();
    
  }
  
  
}
