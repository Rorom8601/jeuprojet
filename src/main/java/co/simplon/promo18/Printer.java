package co.simplon.promo18;

import java.util.Scanner;

public class Printer {

  // Constantes :
  // écrire en vert
  static final String VERT = "\u001B[32m";
  // écrire en jaune
  static final String JAUNE = "\u001B[33m";
  // reset couleurs consoles
  static final String RESET = "\u001B[0m";
  public Scanner scan;
  public WordList wordList;
  // public Wordle wordle=new Wordle();

  public void getScanner() {
    scan = new Scanner(System.in);
  }

  public void startNewLine() {
    System.out.println();
  }

  public static String writeInGreen(char ch) {
    return (VERT + ch + RESET);
  }

  public static String writeInYellow(char ch) {
    return (JAUNE + ch + RESET);
  }

  public void eraseConsole() {
    System.out.println("\033[H\033[2J");
  }

  public void getWordToGuess(GameMode gameMode, Wordle wordle) {
    String word = "";
    if (gameMode.TwoPLayers == true) {
      if (scan == null) {
        getScanner();
      }
      word = scan.nextLine().toLowerCase();
      // Verif sur taille et type
      while ((word.length() != 5) || !(word.matches("[a-zA-Z]+"))) {
        System.out.println("Le mot entré ne contient pas 5 lettres ou des chiffres.");
        word = scan.nextLine().toLowerCase();
      }
    }
    if (gameMode.TwoPLayers == false) {
      if (wordList == null) {
        wordList = new WordList();
      }
      wordList.getListOfFiveLetterWordFromFile();
      wordList.getRandomNumberFromList();
      word = wordList.chosenWord;
    }

    // Verif sur type
    wordle.wordToGuess = word;
    wordle.tabWordToGuess = word.toCharArray();
    // Effacer la console
    this.eraseConsole();
    System.out.println("Le mot a bien été enregistré");
  }

  public void checkDefeatCondition(Wordle wordle) {
    if (wordle.nbTry == 7) {
      System.out.println("\nVous avez perdu, sorry so!!!");
      System.out.println("Le mot à deviner était: " + wordle.wordToGuess);
      this.askToPlayAgain(wordle);
    }
  }

  public void printStartMessage(GameMode gameMode) {

    gameMode.selectGameMode();
    if (gameMode.TwoPLayers == true) {
      System.out.println("Vous avez choisi de jouer à deux");
      System.out
          .println("Entrez un mot français de 5 lettres que votre adversaire devra deviner :");
    }
    if (gameMode.TwoPLayers == false) {
      System.out.println("Vous avez choisi de jouer seul");

    }
  }

  public void printMessageForPlayer() {
    System.out.println("Vous avez 6 essais pour deviner un mot français de 5 lettres");
    System.out.println(
        "Les lettres en vert sont à la bonne place, les lettres en jaune sont dans le mot, mais à une autre place.");
  }

  public void askToPlayAgain(Wordle wordle) {
    System.out.println("Voulez vous jouer de nouveau?");
    System.out.println("Répondez par o ou n");
    if (scan == null) {
      getScanner();
    }
    String st = scan.nextLine();
    if (st.equals("o")) {
      wordle.nbTry = 1;
      wordle.Start();
    }

    if (st.equals("n")) {
      System.exit(0);
    }
  }

  public void printTryNumber(int nbTry) {
    System.out.println("essai n°" + nbTry + " :");
  }

  public void printReponse(char[] tab) {
    System.out.println("Réponse :");
    for (int i = 0; i < tab.length; i++) {
      // Condition sur les couleurs des lettres
      if (Character.isUpperCase(tab[i]))
        System.out.print(writeInGreen(tab[i]));
      if (!Character.isUpperCase(tab[i]))
        System.out.print(writeInYellow(tab[i]));

    }
  }

  public char[] getTabWordTried(String wordTried) {
    if (scan == null) {
      getScanner();
    }
    wordTried = scan.nextLine().toLowerCase();
    while (wordTried.length() != 5) {
      System.out.println("Votre mot proposé ne contient pas 5 lettres");
      wordTried = scan.nextLine();
    }
    char[] tabWordTried = wordTried.toCharArray();
    return tabWordTried;
  }

}
