package co.simplon.promo18;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordList {

  public List<String> frenchWordList = new ArrayList<String>();
  public int max;
  public String chosenWord;

  public void getListOfFiveLetterWordFromFile() {
    BufferedReader reader;
    try {
      reader = new BufferedReader(new FileReader(
          "liste_francais.txt"));
      String line = reader.readLine();
      while (line != null) {
        // System.out.println(line);
        // read next line
        // n'ajouter que les mots de 5 lettres
        if ((line.length() == 5) && (line.matches("[a-z]+")))
        frenchWordList.add(line);
        line = reader.readLine();

      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  private void getSizeOfList(){
    this.max=frenchWordList.size();
  }

  public void getRandomNumberFromList(){
    int min=0;
    this.getSizeOfList();
    int randomNumber= (int)Math.floor(Math.random()*(max-min+1)+min);
    //System.out.println(randomNumber);
    chosenWord=this.frenchWordList.get(randomNumber);


  }

}
