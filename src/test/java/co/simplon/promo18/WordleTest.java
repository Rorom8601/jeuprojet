package co.simplon.promo18;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

public class WordleTest {
  @Test
  public void testCheckForLetterInRightPlace() {
    Wordle wordle=new Wordle();
    String wordToGuess = "chien";
    wordle.tabWordToGuess=wordToGuess.toCharArray();
    String wordTried="chats";
    wordle.tabWordTried=wordToGuess.toCharArray();
    char [] test=new char[]{'C','H','*','*','*'};

    wordle.checkForLetterInRightPlace();
    assertArrayEquals(wordle.tabWordGuessedSoFar, test);
    
   
  }

  @Test
  public void testCheckForLetterInWrongPlace() {

  }
  
  @Test
  public void testCheckVictoryCondition() {
    Wordle wordle=new Wordle();
    wordle.letterGuessed=0;
    wordle.tabWordGuessedSoFar=new char[5];
    wordle.tabWordTried=new char[5];
    for (int i = 0; i < 5; i++) {
      wordle.tabWordGuessedSoFar[i]='A';   
    }
    assertEquals(wordle.letterGuessed, 5);
  }

  @Test
  public void testIncrementTry() {

  }

  @Test
  public void testInitializeWordGuessedSoFar() {

  }

  @Test
  public void testReinitializeLetterGuessed() {

  }

  @Test
  public void testReinitializeWordToFind() {

  }
}
